using System.Drawing;
namespace ProjectOop
{
    public interface AbstractHandle<T>{
        public void input(int x,int y,int heigth, int width,int radius,string color);
        public void output();

        public float caculateArea();

        public void moveGrahic(int x,int y);

        public void mergerGrahic(T obj);

        public void changeColorObject(string color);
    }
}