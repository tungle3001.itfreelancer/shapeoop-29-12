using System.Drawing;

namespace ProjectOop
{
    public class CircleHandle : AbstractHandle<Circle>
    {
        public Circle circle { get; set; }

 
        public float caculateArea()
        {
            return (float)(circle.radius*this.circle.radius*3.14);
        }

        public void changeColorObject(string color)
        {
           this.circle.color = color;
        }

        public void input(int x, int y, int heigth, int width, int radius,string color)
        {
               this.circle.radius =radius;
               this.circle.setX(x.ToString());
               this.circle.setY(y.ToString());
               this.circle.color = color;

        }

        public void mergerGrahic(Circle obj)
        {
           ShapeCommon.Shapes.Add(obj);
        }

        public void moveGrahic(int x, int y)
        {
           this.circle.setX(x.ToString());
           this.circle.setY(y.ToString());
        }

        public void output()
        {
            Console.WriteLine("X",circle.x);
            Console.WriteLine("Y",circle.y);
            Console.WriteLine("Radius:",circle.radius);
      
        }
    }

}