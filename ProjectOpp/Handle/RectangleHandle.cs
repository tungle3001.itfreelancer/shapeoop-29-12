namespace ProjectOop
{

    public class RectangleHandle : AbstractHandle<Rectangle>
    {
        public Rectangle rectangle { get; set; }
        public float caculateArea()
        {
            return rectangle.length * rectangle.width;
        }

        public void changeColorObject(string color)
        {
            rectangle.color = color;
        }

        public void input(int x, int y, int heigth, int width, int radius, string color)
        {
            rectangle.color = color;
            rectangle.setX(x.ToString());
            rectangle.setY(x.ToString());
            rectangle.width = width;
            rectangle.length = heigth;
        }

        public void mergerGrahic(Rectangle obj)
        {
            ShapeCommon.Shapes.Add(obj);
        }

        public void moveGrahic(int x, int y)
        {
            this.rectangle.x = x;
            this.rectangle.y = y;
        }

        public void output()
        {
            Console.WriteLine("X:", this.rectangle.x);
            Console.WriteLine("Y:", this.rectangle.y);
            Console.WriteLine("Width:", this.rectangle.width);
            Console.WriteLine("Length:", this.rectangle.length);

        }
    }
}