namespace ProjectOop
{
    public class ShapeCommon{
        private static List<Shape> shapes = new List<Shape>();

        public static List<Shape> Shapes { get => shapes; set => shapes = value; }
    }
    
}