namespace ProjectOop
{
    public class Rectangle : Shape
    {
        public Rectangle(int x, int y, int length, int width) : base(x, y)
        {
            this.setX(x.ToString());
            this.setY(y.ToString());
            this.length = length;
            this.width = width;
        }
        public int length { get; set; }
        public int width { get; set; }
        public Rectangle(int x, int y) : base(x, y)
        {
            this.setX(x.ToString());
            this.setY(y.ToString());
        }
    
    }

}