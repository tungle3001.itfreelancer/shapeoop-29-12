using System.Drawing;
namespace ProjectOop
{
    public class Triangle : Shape
    {
        public Triangle(int x, int y) : base(x, y)
        {
        
        }

        public Point point1{get;set;}

        public Point point2{get;set;}
    }

}